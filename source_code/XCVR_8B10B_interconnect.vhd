--if you want to modify channel
--just modify component of transceiver(xcvr)

--!! warning !!
--user need to see "DataStruct_param_def_header.vhd"
--most important of typedef and parameter in here
--!! warning !!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

library work;
use work.DataStruct_param_def_header.all;--invoke our defined type and parameter

entity XCVR_8B10B_interconnect is
    port (
        RST_N                       : in  std_logic := '0' ;

        TX_para_external_ch         : in  para_data_men;
        RX_para_external_ch         : out para_data_men;
        TX_para_external_clk_ch     : out ser_data_men;
        RX_para_external_clk_ch     : out ser_data_men;  
        tx_traffic_ready_ext_ch     : out ser_data_men;
        rx_traffic_ready_ext_ch     : out ser_data_men;
        error_cnt_ch                : out para_data_men;

        INIT_Clock                  : in  std_logic;
        Ref_Clock                   : in  std_logic;
     
        RX_ser                      : in  ser_data_men;
        TX_ser                      : out ser_data_men
    );
end entity XCVR_8B10B_interconnect ;    


architecture Top of XCVR_8B10B_interconnect is
    --rst
    signal reset_n                : std_logic := '0';
    --clock
    signal XCVR_Tx_clk_out_ch     : ser_data_men := (others =>'0');
    signal XCVR_Rx_clk_out_ch     : ser_data_men := (others =>'0'); 
    --==================--
    ----data gen/check----
    --==================--
    --loopback_en
    signal internal_loopback_en_ch       : ser_data_men := (others =>'0');

    --para data    
    signal tx_Para_data_ch               : para_data_men;
    signal rx_Para_data_ch               : para_data_men;

    signal tx_Para_data_external_buf_ch  : para_data_men;
    signal rx_Para_data_external_buf_ch  : para_data_men;

    signal tx_Para_data_internal_buf_ch  : para_data_men;
    signal rx_Para_data_internal_buf_ch  : para_data_men;

    signal tx_Para_data_internal_ch      : para_data_men;
    signal rx_Para_data_internal_ch      : para_data_men;

    signal xcvr_tx_Para_data_ch          : para_data_men;
    signal xcvr_rx_Para_data_ch          : para_data_men;

    --ext ctrl

    --unknow
    signal rx_diff_ch               : para_data_men;
    signal rx_pattern_ch            : para_data_men;
    
    --ready

    signal TX_gen_ready_ch          : ser_data_men := (others =>'0');       
    signal RX_chk_ready_ch          : ser_data_men := (others =>'0');           
    signal TX_traffic_ready_ch      : ser_data_men := (others =>'0');
    signal RX_traffic_ready_ch      : ser_data_men := (others =>'0');

    signal tx_gen_ready_internal_ch      : ser_data_men := (others =>'0');
    signal rx_chk_ready_internal_ch      : ser_data_men := (others =>'0');
    signal tx_traffic_ready_internal_ch  : ser_data_men := (others =>'0');
    signal rx_traffic_ready_internal_ch  : ser_data_men := (others =>'0');
    --===============--
    ----transceiver----
    --===============--
    
    --ser data 
    signal Tx_ser_buf               : ser_data_men := (others =>'0');
    signal Rx_ser_buf               : ser_data_men := (others =>'0');

    --controlled data
    signal rx_disp_err_ch           : ctrl_code_8B10B;
    signal rx_err_detec_ch          : ctrl_code_8B10B; 

    signal rx_data_k_ch             : ctrl_code_8B10B; 
    signal tx_data_k_ch             : ctrl_code_8B10B; 

    signal rx_patterndetect_ch      : ctrl_code_8B10B; --arria10
    signal rx_syncstatus_ch         : ctrl_code_8B10B; --arria10
    signal rx_runningdisp_ch        : ctrl_code_8B10B; --arria10
    signal tx_dispval_ch            : ctrl_code_8B10B; --arria10
    signal tx_forcedisp_ch          : ctrl_code_8B10B; --arria10
    --===============--
    --for Stratix-4  --
    --===============--
    
    --reconfig
    signal reconfig_from_gxb        : std_logic_vector ((17 - 1) downto 0) := (others => '0');
    signal reconfig_to_gxb          : std_logic_vector ((4 - 1) downto 0) := (others => '0');
    --Xcvr
    signal XCVR_TxRx_rst            : ser_data_men := (others =>'0');

    signal rx_enapatternalign       : ser_data_men := (others =>'0');

    signal pll_locked               : std_logic ;--stratix4
    signal rx_freqlocked_ch         : ser_data_men := (others =>'0');--stratix4
    --to XCVR Buffer (stratix-4)
    signal pll_locked_from_XCVR         : std_logic_vector(0 downto 0) ;--stratix4

    signal tx_data_k_toXCVR             : std_logic_vector((ctrl_code_length_per_ch*num_of_xcvr_ch -1) downto 0);
    signal xcvr_tx_Para_data_to_XCVR    : std_logic_vector((para_data_length_per_ch*num_of_xcvr_ch -1) downto 0); 

    signal rx_data_k_ch_from_XCVR       : std_logic_vector((ctrl_code_length_per_ch*num_of_xcvr_ch -1) downto 0);
    signal xcvr_rx_Para_data_from_XCVR  : std_logic_vector((para_data_length_per_ch*num_of_xcvr_ch -1) downto 0); 
    signal rx_disp_err_from_XCVR        : std_logic_vector((ctrl_code_length_per_ch*num_of_xcvr_ch -1) downto 0);
    signal rx_err_detec_from_XCVR       : std_logic_vector((ctrl_code_length_per_ch*num_of_xcvr_ch -1) downto 0);
    --===============--
    ----for opensrc----
    --===============--
    type para_data_men_opensrc is array ((num_of_xcvr_ch - 1) downto 0) of 
        std_logic_vector (para_data_length_per_ch_opensrc -1 downto 0); 
    signal from_xcvr_Rx_opensrc         : para_data_men_opensrc ;
    signal to_xcvr_Tx_opensrc           : para_data_men_opensrc ;
    signal xcvr_tx_Para_data_to_XCVR_opensrc        : std_logic_vector(para_data_length_per_ch_opensrc*num_of_xcvr_ch -1 downto 0);
    signal xcvr_rx_Para_data_from_XCVR_opensrc      : std_logic_vector(para_data_length_per_ch_opensrc*num_of_xcvr_ch -1 downto 0);
    signal rx_syncstatus_from_XCVR      : std_logic_vector((ctrl_code_length_per_ch*num_of_xcvr_ch -1) downto 0);
    signal rx_patterndetect_from_XCVR   : std_logic_vector((ctrl_code_length_per_ch*num_of_xcvr_ch -1) downto 0);
    
begin

    --connecte rst 
    reset_n  <= RST_N;

    --connecte ser data
    RX_ser_buf  <=  Rx_ser;
    Tx_ser      <=  Tx_ser_buf ;

    --connect loopback_en
    internal_loopback_en_ch <= (others =>'1') when xcvr_ser_internal_loopback_en = '1' else (others =>'0');

    --connecte para data with internal/external selector
    tx_Para_data_ch <= tx_Para_data_external_buf_ch when scr_para_Data_gen_check_form_this_module = '0';
    tx_Para_data_ch <= tx_Para_data_internal_buf_ch when scr_para_Data_gen_check_form_this_module = '1';   

    rx_Para_data_external_buf_ch <= rx_Para_data_ch when scr_para_Data_gen_check_form_this_module = '0';
    rx_Para_data_internal_buf_ch <= rx_Para_data_ch when scr_para_Data_gen_check_form_this_module = '1';
    
    tx_Para_data_internal_buf_ch <= tx_Para_data_internal_ch when scr_para_Data_gen_check_form_this_module = '1';
    tx_Para_data_external_buf_ch <= TX_para_external_ch      when scr_para_Data_gen_check_form_this_module = '0';

    rx_Para_data_internal_ch <= rx_Para_data_internal_buf_ch when scr_para_Data_gen_check_form_this_module = '1';
    RX_para_external_ch      <= rx_Para_data_external_buf_ch when scr_para_Data_gen_check_form_this_module = '0';

    TX_para_external_clk_ch <= XCVR_Tx_clk_out_ch when scr_para_Data_gen_check_form_this_module = '0';
    RX_para_external_clk_ch <= XCVR_Rx_clk_out_ch when scr_para_Data_gen_check_form_this_module = '0';
    --ready-ext
    rx_traffic_ready_ext_ch <= RX_traffic_ready_ch when scr_para_Data_gen_check_form_this_module = '0';
    tx_traffic_ready_ext_ch <= TX_traffic_ready_ch when scr_para_Data_gen_check_form_this_module = '0';
    --ready-int
    rx_traffic_ready_internal_ch     <= RX_traffic_ready_ch      when scr_para_Data_gen_check_form_this_module = '1';
    tx_traffic_ready_internal_ch     <= TX_traffic_ready_ch      when scr_para_Data_gen_check_form_this_module = '1';
    --to XCVR Buffer connect ( cast from <ctrl_code_8B10B> or <para_data_men> to std_logic_vector<(data_length*(i+1) -1) downto data_length*i> ) 
    XCVR_Buffer_connect : for i in 0 to (num_of_xcvr_ch - 1) generate
        --tx
        --xcvr_tx_Para_data_to_XCVR(((i+1)*para_data_length_per_ch -1) downto (i*para_data_length_per_ch))  <= xcvr_tx_Para_data_ch(i) ; --normal
        --tx_data_k_toXCVR(((i+1)*ctrl_code_length_per_ch -1) downto (i*ctrl_code_length_per_ch))           <= tx_data_k_ch(i);--normal
        xcvr_tx_Para_data_to_XCVR_opensrc(((i+1)*para_data_length_per_ch_opensrc -1) downto (i*para_data_length_per_ch_opensrc))  <= to_xcvr_Tx_opensrc(i) ; --opensrc

        --rx
        --xcvr_rx_Para_data_ch(i)  <= xcvr_rx_Para_data_from_XCVR(((i+1)*para_data_length_per_ch -1) downto (i*para_data_length_per_ch)); --normal
        --rx_data_k_ch(i)          <= rx_data_k_ch_from_XCVR(((i+1)*ctrl_code_length_per_ch -1) downto (i*ctrl_code_length_per_ch));--normal
        --rx_err_detec_ch(i)       <= rx_disp_err_from_XCVR(((i+1)*ctrl_code_length_per_ch -1) downto (i*ctrl_code_length_per_ch));--normal
        --rx_disp_err_ch(i)        <= rx_err_detec_from_XCVR(((i+1)*ctrl_code_length_per_ch -1) downto (i*ctrl_code_length_per_ch));--normal
        rx_syncstatus_ch(i)      <= rx_syncstatus_from_XCVR(((i+1)*ctrl_code_length_per_ch -1) downto (i*ctrl_code_length_per_ch));
        rx_patterndetect_ch(i)   <= rx_patterndetect_from_XCVR(((i+1)*ctrl_code_length_per_ch -1) downto (i*ctrl_code_length_per_ch));
        from_xcvr_Rx_opensrc(i)  <= xcvr_rx_Para_data_from_XCVR_opensrc(((i+1)*para_data_length_per_ch_opensrc -1) downto (i*para_data_length_per_ch_opensrc)); --opensrc
    end generate XCVR_Buffer_connect;
    --connect PLL locked (cast from <std_logic_vector(0 downto 0)> to <std_logic>)
    pll_locked <= pll_locked_from_XCVR(0);
    --XCVR module connect 
    XCVR : entity work.XCVR_3125_4ch
        port map(
            cal_blk_clk		    => Ref_Clock,
            pll_inclk		    => Ref_Clock,
            reconfig_clk		=> Ref_Clock,
            reconfig_togxb		=> reconfig_to_gxb,
            rx_coreclk		    => XCVR_Rx_clk_out_ch,
            rx_cruclk		    => (others => Ref_Clock),
            rx_datain		    => RX_ser_buf,
            rx_digitalreset		=> XCVR_TxRx_rst,
            rx_enapatternalign	=> rx_enapatternalign,  
            rx_seriallpbken		=> internal_loopback_en_ch,
            tx_coreclk		    => XCVR_Tx_clk_out_ch,
            tx_datain		    => xcvr_tx_Para_data_to_XCVR_opensrc,
            tx_digitalreset		=> XCVR_TxRx_rst, 
            pll_locked		    => pll_locked_from_XCVR,
            reconfig_fromgxb	=> reconfig_from_gxb,
            rx_clkout		    => XCVR_Rx_clk_out_ch,
            rx_dataout		    => xcvr_rx_Para_data_from_XCVR_opensrc,
            rx_freqlocked		=> rx_freqlocked_ch,
            rx_patterndetect	=> rx_patterndetect_from_XCVR,
            rx_syncstatus		=> rx_syncstatus_from_XCVR,
            tx_clkout		    => XCVR_Tx_clk_out_ch,
            tx_dataout		    => Tx_ser_buf
        );
    XCVR_reconfig : entity work.XCVR_Reconfig_3125_4ch
    	port map(
            reconfig_clk		=> Ref_Clock,
            reconfig_fromgxb	=> reconfig_from_gxb,
            busy		        => open,
            reconfig_togxb		=> reconfig_to_gxb
        );
    --others connect   
    Data_gen_loop : for i in 0 to (num_of_xcvr_ch - 1) generate
        Data_gen : entity work.frame_gen 
            port map(
                TX_D             => tx_Para_data_internal_ch(i),

                TX_traffic_ready => tx_traffic_ready_internal_ch(i),
                
                USER_CLK         => XCVR_Tx_clk_out_ch(i) ,
                SYSTEM_RESET_N   => reset_n
            );
    end generate Data_gen_loop;   

    Data_check_loop : for i in 0 to (num_of_xcvr_ch - 1) generate
        Data_check : entity work.frame_check
            port map(
                RX_D              => rx_Para_data_internal_ch(i),  

                RX_traffic_ready  => rx_traffic_ready_internal_ch(i),

                RX_errdetect      => rx_err_detec_ch(i),      
                RX_disperr        => rx_disp_err_ch(i),
                rx_freq_locked    => rx_freqlocked_ch(i),

                ERROR_COUNT       => error_cnt_ch(i),      

                USER_CLK          => XCVR_Rx_clk_out_ch(i),     
                SYSTEM_RESET_N    => reset_n      
            );
    end generate Data_check_loop;

    generate_traffic_loop : for i in 0 to (num_of_xcvr_ch - 1) generate
        traffic : entity work.traffic
            port map(
                Reset_n                  => reset_n,

                pll_locked               => pll_locked,--stratix4
                rx_freq_locked           => rx_freqlocked_ch(i),--stratix4

                tx_traffic_ready         => TX_traffic_ready_ch(i),
                rx_traffic_ready         => RX_traffic_ready_ch(i),

                Tx_K                     => tx_data_k_ch(i),
                Rx_K                     => rx_data_k_ch(i),
                TX_DATA_Xcvr             => xcvr_tx_Para_data_ch(i),
                RX_DATA_Xcvr             => xcvr_rx_Para_data_ch(i),
                Tx_DATA_client           => tx_Para_data_ch(i),
                Rx_DATA_client           => rx_Para_data_ch(i),

                RX_errdetect             => rx_err_detec_ch(i),
                RX_disperr               => rx_disp_err_ch(i),

                rx_sync_status           => rx_syncstatus_ch(i),
                rx_pattern_detected      => rx_patterndetect_ch(i),

                XCVR_Manual_rst          => XCVR_TxRx_rst(i),                
                rx_align_en              => rx_enapatternalign(i), --stratix4

                INIT_CLK                 => Ref_Clock,
                
                Tx_Clk                   => XCVR_Tx_clk_out_ch(i),
                Rx_Clk                   => XCVR_Rx_clk_out_ch(i)
            );
    end generate generate_traffic_loop;

    generate_16B20B_enc_loop : for i in 0 to (num_of_xcvr_ch - 1) generate
        enc : entity work.encoder_16b20b
            port map(
                data_in  => xcvr_tx_Para_data_ch(i),
                disp_in  => tx_data_k_ch(i),

                data_out => to_xcvr_Tx_opensrc(i)
            );
    end generate generate_16B20B_enc_loop;

    generate_16B20B_dec_loop : for i in 0 to (num_of_xcvr_ch - 1) generate
        enc : entity work.decoder_16b20b
            port map(
                data_in  => from_xcvr_Rx_opensrc(i),

                data_out => xcvr_rx_Para_data_ch(i),
                disp_out => rx_data_k_ch(i),

                code_err => rx_err_detec_ch(i),
                disp_err => rx_disp_err_ch(i)
            );
end generate generate_16B20B_dec_loop;
end architecture Top;